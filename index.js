function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave");
  var txtValor = document.getElementById('txtValor');
  var clave = txtClave.value;
  var valor = txtValor.value;
  localStorage.setItem(clave,valor);

  var valor = {
  "nombre":"JLuis",
  "Apellido":"Cruz",
  "Ciudad":"Madrid",
  "Pais":"España"
  };

  localStorage.setItem("json",JSON.stringify(valor));
}

function leerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave");
  var clave = txtClave.value;
  var valor = localStorage.getItem(clave);

  var spanValor = document.getElementById('spanValor');
  spanValor.innerText = valor;

  var datosUsuario = JSON.parse(localStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.Ciudad);
  console.log(datosUsuario)
}
